From: Andrej Shadura <andrewsh@debian.org>
Date: Wed, 9 Feb 2022 22:17:55 +0100
Subject: Revert "Remove spotify controller (#535)"

This reverts commit 72c3e36c2a9a4e7dd62bc4944d5fd2c172b1acd8.
---
 examples/spotify_example.py         | 120 ++++++++++++++++++++++++++++++++++++
 pychromecast/config.py              |   1 +
 pychromecast/controllers/spotify.py | 100 ++++++++++++++++++++++++++++++
 pychromecast/quick_play.py          |   3 +
 4 files changed, 224 insertions(+)
 create mode 100644 examples/spotify_example.py
 create mode 100644 pychromecast/controllers/spotify.py

diff --git a/examples/spotify_example.py b/examples/spotify_example.py
new file mode 100644
index 0000000..92ed8ed
--- /dev/null
+++ b/examples/spotify_example.py
@@ -0,0 +1,120 @@
+"""
+Example on how to use the Spotify Controller.
+NOTE: You need to install the spotipy and spotify-token dependencies.
+
+This can be done by running the following:
+pip install spotify-token
+pip install git+https://github.com/plamere/spotipy.git
+"""
+# pylint: disable=invalid-name
+
+import argparse
+import logging
+import time
+import sys
+
+import zeroconf
+import spotify_token as st  # pylint: disable=import-error
+import spotipy  # pylint: disable=import-error
+
+import pychromecast
+from pychromecast.controllers.spotify import SpotifyController
+
+CAST_NAME = "My Chromecast"
+
+parser = argparse.ArgumentParser(
+    description="Example on how to use the Spotify Controller."
+)
+parser.add_argument(
+    "--cast", help='Name of cast device (default: "%(default)s")', default=CAST_NAME
+)
+parser.add_argument(
+    "--known-host",
+    help="Add known host (IP), can be used multiple times",
+    action="append",
+)
+parser.add_argument("--show-debug", help="Enable debug log", action="store_true")
+parser.add_argument(
+    "--show-zeroconf-debug", help="Enable zeroconf debug log", action="store_true"
+)
+parser.add_argument("--user", help="Spotify username", required=True)
+parser.add_argument("--password", help="Spotify password", required=True)
+parser.add_argument(
+    "--uri",
+    help='Spotify uri(s) (default: "%(default)s")',
+    default=["spotify:track:3Zwu2K0Qa5sT6teCCHPShP"],
+    nargs="+",
+)
+args = parser.parse_args()
+
+if args.show_debug:
+    logging.basicConfig(level=logging.DEBUG)
+    # Uncomment to enable http.client debug log
+    # http_client.HTTPConnection.debuglevel = 1
+if args.show_zeroconf_debug:
+    print("Zeroconf version: " + zeroconf.__version__)
+    logging.getLogger("zeroconf").setLevel(logging.DEBUG)
+
+chromecasts, browser = pychromecast.get_listed_chromecasts(
+    friendly_names=[args.cast], known_hosts=args.known_host
+)
+cast = list(chromecasts)[0]
+
+if not cast:
+    print('No chromecast with name "{}" discovered'.format(args.cast))
+    print("Discovered casts: {}".format(chromecasts))
+    sys.exit(1)
+
+print("cast {}".format(cast))
+
+
+# Wait for connection to the chromecast
+cast.wait()
+
+spotify_device_id = None
+
+# Create a spotify token
+data = st.start_session(args.user, args.password)
+access_token = data[0]
+expires = data[1] - int(time.time())
+
+# Create a spotify client
+client = spotipy.Spotify(auth=access_token)
+if args.show_debug:
+    spotipy.trace = True
+    spotipy.trace_out = True
+
+# Launch the spotify app on the cast we want to cast to
+sp = SpotifyController(access_token, expires)
+cast.register_handler(sp)
+sp.launch_app()
+
+if not sp.is_launched and not sp.credential_error:
+    print("Failed to launch spotify controller due to timeout")
+    sys.exit(1)
+if not sp.is_launched and sp.credential_error:
+    print("Failed to launch spotify controller due to credential error")
+    sys.exit(1)
+
+# Query spotify for active devices
+devices_available = client.devices()
+
+# Match active spotify devices with the spotify controller's device id
+for device in devices_available["devices"]:
+    if device["id"] == sp.device:
+        spotify_device_id = device["id"]
+        break
+
+if not spotify_device_id:
+    print('No device with id "{}" known by Spotify'.format(sp.device))
+    print("Known devices: {}".format(devices_available["devices"]))
+    sys.exit(1)
+
+# Start playback
+if args.uri[0].find("track") > 0:
+    client.start_playback(device_id=spotify_device_id, uris=args.uri)
+else:
+    client.start_playback(device_id=spotify_device_id, context_uri=args.uri[0])
+
+# Shut down discovery
+browser.stop_discovery()
diff --git a/pychromecast/config.py b/pychromecast/config.py
index 143a27d..e10ad2e 100644
--- a/pychromecast/config.py
+++ b/pychromecast/config.py
@@ -12,6 +12,7 @@ APP_PLEX = "06ee44ee-e7e3-4249-83b6-f5d0b6f07f34_1"
 APP_DASHCAST = "84912283"
 APP_HOMEASSISTANT_LOVELACE = "A078F6B0"
 APP_HOMEASSISTANT_MEDIA = "B45F4572"
+APP_SPOTIFY = "CC32E753"
 APP_SUPLA = "A41B766D"
 APP_YLEAREENA = "A9BCCB7C"
 APP_BUBBLEUPNP = "3927FA74"
diff --git a/pychromecast/controllers/spotify.py b/pychromecast/controllers/spotify.py
new file mode 100644
index 0000000..ef44f56
--- /dev/null
+++ b/pychromecast/controllers/spotify.py
@@ -0,0 +1,100 @@
+"""
+Controller to interface with Spotify.
+"""
+import logging
+import threading
+
+from . import BaseController
+from ..config import APP_SPOTIFY
+from ..error import LaunchError
+
+APP_NAMESPACE = "urn:x-cast:com.spotify.chromecast.secure.v1"
+TYPE_GET_INFO = "getInfo"
+TYPE_GET_INFO_RESPONSE = "getInfoResponse"
+TYPE_SET_CREDENTIALS = "setCredentials"
+TYPE_SET_CREDENTIALS_ERROR = "setCredentialsError"
+TYPE_SET_CREDENTIALS_RESPONSE = "setCredentialsResponse"
+
+
+# pylint: disable=too-many-instance-attributes
+class SpotifyController(BaseController):
+    """Controller to interact with Spotify namespace."""
+
+    def __init__(self, access_token=None, expires=None):
+        super().__init__(APP_NAMESPACE, APP_SPOTIFY)
+
+        self.logger = logging.getLogger(__name__)
+        self.session_started = False
+        self.access_token = access_token
+        self.expires = expires
+        self.is_launched = False
+        self.device = None
+        self.credential_error = False
+        self.waiting = threading.Event()
+
+    def receive_message(self, _message, data: dict):
+        """
+        Handle the auth flow and active player selection.
+
+        Called when a message is received.
+        """
+        if data["type"] == TYPE_SET_CREDENTIALS_RESPONSE:
+            self.send_message({"type": TYPE_GET_INFO, "payload": {}})
+        if data["type"] == TYPE_SET_CREDENTIALS_ERROR:
+            self.device = None
+            self.credential_error = True
+            self.waiting.set()
+        if data["type"] == TYPE_GET_INFO_RESPONSE:
+            self.device = data["payload"]["deviceID"]
+            self.is_launched = True
+            self.waiting.set()
+        return True
+
+    def launch_app(self, timeout=10):
+        """
+        Launch Spotify application.
+
+        Will raise a LaunchError exception if there is no response from the
+        Spotify app within timeout seconds.
+        """
+
+        if self.access_token is None or self.expires is None:
+            raise ValueError("access_token and expires cannot be empty")
+
+        def callback():
+            """Callback function"""
+            self.send_message(
+                {
+                    "type": TYPE_SET_CREDENTIALS,
+                    "credentials": self.access_token,
+                    "expiresIn": self.expires,
+                }
+            )
+
+        self.device = None
+        self.credential_error = False
+        self.waiting.clear()
+        self.launch(callback_function=callback)
+
+        counter = 0
+        while counter < (timeout + 1):
+            if self.is_launched:
+                return
+            self.waiting.wait(1)
+            counter += 1
+
+        if not self.is_launched:
+            raise LaunchError(
+                "Timeout when waiting for status response from Spotify app"
+            )
+
+    # pylint: disable=too-many-locals
+    def quick_play(self, **kwargs):
+        """
+        Launches the spotify controller and returns when it's ready.
+        To actually play media, another application using spotify connect is required.
+        """
+        self.access_token = kwargs["access_token"]
+        self.expires = kwargs["expires"]
+
+        self.launch_app(timeout=20)
diff --git a/pychromecast/quick_play.py b/pychromecast/quick_play.py
index 1226801..ef6c101 100644
--- a/pychromecast/quick_play.py
+++ b/pychromecast/quick_play.py
@@ -3,6 +3,7 @@
 from .controllers.youtube import YouTubeController
 from .controllers.supla import SuplaController
 from .controllers.yleareena import YleAreenaController
+from .controllers.spotify import SpotifyController
 from .controllers.bubbleupnp import BubbleUPNPController
 from .controllers.bbciplayer import BbcIplayerController
 from .controllers.bbcsounds import BbcSoundsController
@@ -58,6 +59,8 @@ def quick_play(cast, app_name, data):
         controller = SuplaController()
     elif app_name == "yleareena":
         controller = YleAreenaController()
+    elif app_name == "spotify":
+        controller = SpotifyController()
     elif app_name == "bubbleupnp":
         controller = BubbleUPNPController()
     elif app_name == "bbciplayer":
