Source: pychromecast
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Ruben Undheim <ruben.undheim@gmail.com>
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-setuptools,
               python3-protobuf,
               python3-requests,
               python3-zeroconf (>= 0.25.1~)
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/python-team/packages/pychromecast
Vcs-Git: https://salsa.debian.org/python-team/packages/pychromecast.git
Homepage: https://github.com/home-assistant-libs/pychromecast
Rules-Requires-Root: no

Package: python3-pychromecast
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends},
         python3-zeroconf,
         python3-protobuf,
         python3-requests,
Description: Library to communicate with Google Chromecast (Python 3)
 This library makes it easy to communicate with a Chromecast device using
 Python.
 .
 It currently supports:
 .
  - Auto discovering connected Chromecasts on the network
  - Start the default media receiver and play any online media
  - Control playback of current playing media
  - Implement Google Chromecast API v2
  - Communicate with apps via channels
  - Easily extendable to add support for unsupported namespaces
 .
 This package contains the Python 3 version of pychromecast
